* Installation

Download the current build file (~.xpi~) from [[https://gitlab.com/maxburon/ydla-add-on/-/releases][this page]].

For the moment, the [[https://support.mozilla.org/fr/kb/signature-modules-complementaires-firefox?as=u&utm_source=inproduct][extension is not signed]], so you have to desactivate the signature requirement for the installation: 
- open ~about:config~ in a new tab,
- set ~xpinstall.signatures.required~ to false.


* Building

Execute the following command:
#+BEGIN_src shell
npm i --global web-ext
web-ext build 
#+END_src

