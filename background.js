let currentTab;
const bookmarks = [];
let status = 'ok';

/*
 * Updates the browserAction icon to reflect whether the current page
 * is already bookmarked.
 */
function updateIcon() {
  let icon = 'icons/logo-white.svg'
  let title = 'Bookmark it !'
  switch (status) {
  case 'ok':
    icon = 'icons/logo-white.svg'
    break;
  case 'bookmarked':
    icon = 'icons/logo.svg'
    title = 'Bookmarked'
    break;
  default:
    icon = 'icons/logo-red.svg'
    title = 'Error :/'
  }
  
  browser.browserAction.setIcon({
    path: {
      38: icon
    } ,
    tabId: currentTab.id
  });
  browser.browserAction.setTitle({
    // Screen readers can see the title
    title: title,
    tabId: currentTab.id
  }); 
}

/*
 * Add or remove the bookmark on the current page.
 */
function toggleBookmark() {

  const url = 'https://medias-dl.buron.coffee/api/medias'
  const bookmark = currentTab.url
  const data = {
    url: bookmark,
    withdownload: false
  }

  console.log(data)
  fetch(url, {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers: {
      'Content-Type': 'application/json'
    },
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer', // no-referrer, *client
    body: JSON.stringify(data) // body data type must match "Content-Type" header
  }).then(res => {
    bookmarks.push(bookmark);

    if (res.status === 200) {
      status = 'bookmarked'
    } else {
      status = 'error'
    }
    updateIcon();
  })
}

browser.browserAction.onClicked.addListener(toggleBookmark);

/*
 * Switches currentTab and currentBookmark to reflect the currently active tab
 */
function updateActiveTab(tabs) {

  function isSupportedProtocol(urlString) {
    var supportedProtocols = ["https:", "http:", "ftp:", "file:"];
    var url = document.createElement('a');
    url.href = urlString;
    return supportedProtocols.indexOf(url.protocol) != -1;
  }

  function updateTab(tabs) {
    if (tabs[0]) {
      currentTab = tabs[0];
      if (isSupportedProtocol(currentTab.url)) {
        status = (bookmarks.includes(currentTab.url)) ? 'bookmarked' : 'ok';
        updateIcon();
      } else {
        console.log(`Bookmark it! does not support the '${currentTab.url}' URL.`)
      }
    }
  }

  var gettingActiveTab = browser.tabs.query({active: true, currentWindow: true});
  gettingActiveTab.then(updateTab);
}

// listen to tab URL changes
browser.tabs.onUpdated.addListener(updateActiveTab);

// listen to tab switching
browser.tabs.onActivated.addListener(updateActiveTab);

// listen for window switching
browser.windows.onFocusChanged.addListener(updateActiveTab);

// update when the extension loads initially
updateActiveTab();
